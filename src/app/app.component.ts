import { Component } from "@angular/core";

import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { UrlService } from "src/providers/url-service";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { Network } from "@ionic-native/network/ngx";
import { ServicesAlertsProviderService } from "src/providers/services-alerts-provider.service";
import { Subscription } from "rxjs";
import Util from "../constants/content";
import { AuthGuard } from "src/providers/authGuard";
import Swal from 'sweetalert2';

@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  rootPage: any = "";
  lastLoggedOutTime: any;
  internetStatus;
  desgination;
  appVersion;
  profilePhoto;
  mobile;
  name;
  token;
  Unsubscribe: Subscription;
  currentVersion: any;

  syncTitle = "LastSigned:";
  pages: Array<{ title: string; component: any; icon: string }>;
  userToken;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private urlService: UrlService,
    private route: Router,
    private network: Network,
    private alertService: ServicesAlertsProviderService,
    public storage: Storage,
    private navCtrl: NavController,
    private authGurad: AuthGuard
  ) {
    this.initializeApp();

    this.Unsubscribe = this.urlService.profileDetails.subscribe(profile => {
      this.desgination = profile.mail;
      this.appVersion = profile.version;
      this.lastLoggedOutTime = profile.laslogged_out_time;
      this.token = profile.token;
      this.name = profile.name;
      this.mobile = profile.mobile;
      this.playStoreUpdateCheck();
      console.log("username", profile.token);
    });
  }

  initializeApp() {
    setTimeout(() => {
      this.splashScreen.hide();
      this.authGurad.authenticated.subscribe(async (state: boolean) => {
        if (state) {
          console.log("appcompe", state);
         
          this.navCtrl.navigateRoot(['/dashboard']);
        } else {
          this.navCtrl.navigateRoot(["/login"]);
        }
      });
    }, 500);
  }
  openProfile() {
    this.route.navigate(["/profilepage"]);
  }
  
  homePage() {
    this.route.navigate(["/dashboard"]);
  }
  profilePage() {
    this.route.navigate(["/profilepage"]);
    // this.navCtrl.popToRoot();
  }
  Logout() {
    if (this.network.type != "none") {
      this.urlService.logoutFunc();
      this.route.navigate(["/login"]);
    } else {
      this.alertService.showToast(Util.OFFLINE_CONTENT, Util.TOAST_DURATION);
    }
  }

  ionViewDidLeave() {
    this.Unsubscribe.unsubscribe();
  }

  /**
   * Get Profile Details from Storage
   */
  async getProfileDetails() {
    await this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
      const prof = await JSON.parse(profile);
      if (prof) {
        if (prof.profiledetails) {
          this.appVersion = await prof.profiledetails.version;
          this.lastLoggedOutTime = prof.profiledetails.laslogged_out_time;
          this.desgination = prof.profiledetails.mail;
          this.token = prof.profiledetails.token;
          this.name = prof.profiledetails.name;
          this.mobile = prof.profiledetails.mobile;
          console.log("appcomponent get profile", this.token);
        }
      }
    });
  }

  

  /**
   * Check in playstore App version  changed or Not
   */
  async playStoreUpdateCheck() {
    debugger;
    if (this.network.type != "none") {
      this.urlService
        .onServerCheck()
        .then(async (result: boolean) => {
          if (result === true) {
            this.alertService.dismissLoading();
            await this.urlService.onCurrentVersionDetails().then((res: any) => {
              this.currentVersion = res;
              console.log('storefunction', this.appVersion);
              const oldVersion = this.appVersion || this.urlService.profile.version;
              debugger;
              if (parseFloat(this.currentVersion) > parseFloat(oldVersion)) {
                this.playStoreAlertMessage();
              }
            });
          } else {
           // this.navCtrl.navigateRoot(["/dashboard"]);
          }
        })
        .catch(err => {
          this.alertService.dismissLoading();
          //this.navCtrl.navigateRoot(["/dashboard"]);
        });
    } else {
      //this.navCtrl.navigateRoot(["/dashboard"]);
    }
  }
  playStoreAlertMessage() {

    Swal.fire({
      title: '<strong> TARA </strong>',
      type: 'info',
      html: '<strong> <b>Kindly update App in Google Play Store!</b> </strong>',
      showCancelButton: true,
      confirmButtonText: 'Yes, update!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        window.open('https://play.google.com/store/apps/details?id=com.iitm.tara', '_system');
      } else {
     //  result.dismiss === Swal.DismissReason.cancel
       Swal.DismissReason.cancel;
       
      }
    })
  }
}
