import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-radio-c',
  templateUrl: './radio-c.component.html',
  styleUrls: ['./radio-c.component.scss'],
})
export class RadioCComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('bindValue') value: any;
  @Input('options') values: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {}
  onChangeObj(event) {
    this.onFocusO.emit(this.value)
  }
}
