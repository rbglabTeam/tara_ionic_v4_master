import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import Util from '../../constants/content';
@Component({
  selector: 'anyone-c',
  templateUrl: './anyone-c.component.html',
  styleUrls: ['./anyone-c.component.scss'],
})
export class AnyoneCComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('options') values: any;
  @Input('bindValue') value: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {}
  onChangeObj(event) {
    this.onFocusO.emit(this.value)
  }

}
