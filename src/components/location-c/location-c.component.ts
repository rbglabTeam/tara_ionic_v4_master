import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { ServicesAlertsProviderService } from "src/providers/services-alerts-provider.service";
import Util from "../../constants/content";


@Component({
  selector: "location-c",
  templateUrl: "./location-c.component.html"
})
export class LocationCComponent implements OnInit {
  isWeb: boolean = false;
  @Input("labelName") labelName: string;
  @Input("bindValue") value: any;
  @Output("returnValue") onFocusO: EventEmitter<any> = new EventEmitter();
  
  constructor(
    public geolocation: Geolocation,
    private alertService: ServicesAlertsProviderService
  ) {}

  ngOnInit() {}
  
  
  async getGeoLocation() {
    // this.alertService.showLoading(Util.GPS_LOCATION_LOADING_MESSAGE);
    this.geolocation
      .getCurrentPosition({ enableHighAccuracy: true})
      .then(data => {
        //this.alertService.dismissLoading();
        this.value =
          "Lat:" +
          data.coords.latitude +
          "\n" +
          "Long:" +
          data.coords.longitude;

        //  this.value['Lat']= data.coords.latitude;
        //   this.value['Long'] = data.coords.longitude;
        this.onFocusO.emit(this.value);
      })
      .catch(error => {
        this.alertService.dismissLoading();

        console.log("Error getting location", error);
      });
  }
}
