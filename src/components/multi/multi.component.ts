import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multi',
  templateUrl: './multi.component.html',
  styleUrls: ['./multi.component.scss'],
})
export class MultiComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('options') values: any;
  @Input('bindValue') value: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {}
  onChangeObj(event) {
    this.onFocusO.emit(this.value)
  }
}
