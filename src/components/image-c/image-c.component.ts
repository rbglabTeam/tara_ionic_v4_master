import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import {
  Camera,
  CameraOptions,
  PictureSourceType
} from '@ionic-native/camera/ngx';
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from '@ionic-native/file-transfer/ngx';
import { Network } from '@ionic-native/network/ngx';
import { UrlService } from 'src/providers/url-service';
import { ServicesAlertsProviderService } from 'src/providers/services-alerts-provider.service';
import Util from '../../constants/content';
import { StorageService } from 'src/providers/storageServices';
import { WebView } from '@ionic-native/ionic-webview/ngx';



import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'image-c',
  templateUrl: './image-c.component.html',
  styleUrls: ['./image-c.component.scss']
})
export class ImageCComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('bindValue') value: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
   data: any;
  loading = null;

  netStatus = false;
  imageString;
  offline = false;
  sourceType: PictureSourceType;
  constructor(
    private transfer: FileTransfer,
    private network: Network,
    private camera: Camera,
    // private file: File,
    // private filePath: FilePath,
    public photoViewer: PhotoViewer,
    private urlService: UrlService,
    // private platform: Platform,
    private alertService: ServicesAlertsProviderService,
    private storageService: StorageService,
    private webview: WebView
  ) {}

  ngOnInit() {
    if (this.network.type === 'none') {
      this.netStatus = false;
    } else {
      this.netStatus = true;

    }

    this.storageService.networkConnectDisConnectOb.subscribe(data => {
      if (data === 'online') {
        this.netStatus = true;
      }
    });
  }
  /**
   * photo viewer
   */
  onPhotoViewer() {
    console.log('photoviewer', this.value);
    if (this.network.type === 'none') {
    this.photoViewer.show(this.value, this.labelName, {share: true});
   } else {
    
     this.photoViewer.show(this.getImage, this.labelName, {share: true});
   }

  }
 
  /**
   * Even Emitter Emit the Value
   */
  onfocusOut() {
    debugger;
    this.onFocusO.emit(this.value);
  }
/**
 * 
 * @param images Picture Taken
 */
  async pictureFromCamera(images) {
    debugger;
    const text = images.currentTarget.innerText;

    console.log('imagesname', text);
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true,
      saveToPhotoAlbum: true
    };

    this.takePicture(options);
  }

  async takePicture(options: CameraOptions) {
    // this.loading = "Processing please wait..";
    try {
      debugger;
      this.camera.getPicture(options).then(
        async imagePath => {
          if (imagePath > this.urlService.maxFileSize) {
            this.loading = null;
            this.alertService.showToast(
              'Max media size is ' +
                this.urlService.maxFileSize / 1000000 +
                'MB',
              3000
            );
          } else {
            this.loading = null;
            console.log('image path', imagePath);
            this.data = imagePath;
            this.imageString = this.webview.convertFileSrc(imagePath);

            debugger;
            if (this.network.type === 'none') {
              this.offline = true;
              const value = { image: 'jpg', imagepath: this.data };
              this.storageService.onSaveOfflineImage(value);
              this.value = await this.imageString;
              await this.onFocusO.emit(this.data);

            } else {
              this.offline = false;
              await this.onFocusO.emit(this.data);
              this.value = await this.data;
              this.loading = 'Please wait ...';
              await this.uploadFile();
            }
          }
        },
        error => {
          console.log('imagefunction', error);
        }
      );
    } catch (e) {
      console.error(e);
    }
  }

  uploadFile() {
    try {
      this.alertService.showLoading(Util.IMAGES_UPLOADING_MESSAGE);
      const fileTransfer: FileTransferObject = this.transfer.create();

      const options: FileUploadOptions = {
        fileKey: 'image',
        fileName: 'ionic',
        chunkedMode: false,
        mimeType: 'jpg',

        headers: {}
      };
      const filePath = this.value;

      fileTransfer
        .upload(
          filePath,
          Util.SANTHOSH_API + 'media/image?image=' + this.value,
          options
        )
        .then(
          data => {
            this.alertService.dismissLoading();
            this.loading = null;
            this.alertService.showToast('Image uploaded', Util.TOAST_DURATION);
            console.log('imageuploadedResponse', JSON.parse(data.response));
            // this.onFocusO.emit(this.value)
          },
          err => {
            this.loading = null;
            console.log(err);
          }
        );
    } catch (e) {
      console.error(e);
    }
  }
  /***
   * store Images Seperate Folder
   */
  // onStorageSpecificFolder(imagename) {
  //   let imageName = imagename;
  //   const ROOT_DIRECTORY = this.file.externalRootDirectory;

  //   const downloadFolderName = "tempDownloadFolder";
  //   debugger;
  //   //Create a folder in memory location
  //   this.file
  //     .createDir(ROOT_DIRECTORY, downloadFolderName, true)

  //     .then(entries => {
  //       debugger;
  //       console.log("createfolder", entries);
  //       //Copy our asset/img/FreakyJolly.jpg to folder we created
  //       this.file
  //         .copyFile(
  //           this.file.externalRootDirectory + "file:///storage/emulated/0/",
  //           imageName,
  //           ROOT_DIRECTORY + downloadFolderName + "//",
  //           imageName
  //         )
  //         .then(entries => {
  //           console.log("openfolder", entries);
  //           //Open copied file in device's default viewer
  //           this.fileOpener
  //             .open(
  //               ROOT_DIRECTORY + downloadFolderName + "/" + imageName,
  //               "image/jpeg"
  //             )
  //             .then(() => console.log("File is opened"))
  //             .catch(e => alert("Error" + JSON.stringify(e)));
  //         })
  //         .catch(error => {
  //           alert("error " + JSON.stringify(error));
  //         });
  //     })
  //     .catch(error => {
  //       alert("error" + JSON.stringify(error));
  //     });
  // }
  /**
   * imagePath dispalyed Html Page
   */
  get getImage() {
    debugger;

    if (this.network.type != 'none') {
      return Util.SANTHOSH_API + 'media/image?image=' + this.value;
    } else {
      return  'http://localhost/_app_file_/' + this.imageURL(this.value);
    }

  }



  /**
   * Given ImageUrl In Offline Mode
   */
  imageURL(char) {
    debugger;
    const stringPath = char;
    return stringPath.substr(8);
  }
}
