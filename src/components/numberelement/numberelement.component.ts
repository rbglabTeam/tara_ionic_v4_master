import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-numberelement',
  templateUrl: './numberelement.component.html',
  styleUrls: ['./numberelement.component.scss'],
})
export class NumberelementComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('bindValue') value: number;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {}
  onfocusOut() {
    this.onFocusO.emit(this.value)
  }
  _keyPress(event: any) {
    const pattern = /[0-9\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
}
