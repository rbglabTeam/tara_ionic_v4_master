import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stringelement',
  templateUrl: './stringelement.component.html',
  styleUrls: ['./stringelement.component.scss']
})
export class StringelementComponent implements OnInit {

  @Input('labelName') labelName: string;
  
  @Input('bindValue') value: string;
  @Output('returnValue') onFocus: EventEmitter<any> = new EventEmitter();
  constructor() {}
  onfocusOut() {
    this.onFocus.emit(this.value);
  }
  ngOnInit() {}
}
