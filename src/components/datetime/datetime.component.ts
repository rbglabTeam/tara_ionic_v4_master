import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-datetime',
  templateUrl: './datetime.component.html',
  styleUrls: ['./datetime.component.scss'],
})
export class DatetimeComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('bindValue') value: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  public dateOfToday = new Date().toISOString().slice(0, 10);
  constructor() { }

  ngOnInit() {}
  onChangeObj(event) {
    this.onFocusO.emit(this.value)
  }
}
