
export default class Utils {
static API = 'http://localhost:9001/';
// static SANTHOSH_API = 'http://192.168.43.52:9001/';
//static SANTHOSH_API = 'http://10.42.91.176:9001/';
//static SANTHOSH_API = 'http://192.168.0.102:9001/';
//static SANTHOSH_API = 'http://10.42.91.239:9001/';

 static SANTHOSH_API = 'http://13.234.52.55:9002/';
//static SANTHOSH_API = 'http://13.234.52.55:9001/';
//static SANTHOSH_API = 'http://192.168.0.113:9001/';
static LOGIN_LOADING_MESSAGE = 'Please Wait Logging...';
static SERVER_LOADING_MESSAGE = 'Please Wait Server Checking...';
static  API_LOGIN_URL_PARAMS = 'api/user';
static  API_SERVER_REACH_URL_PARAMS = 'api/serverreach';
static  SERVER_NOT_AVAILABLE = 'Please Try Again Server Not Reachable...';
static  TOAST_DURATION = 1000;
static  ONLINE_CONTENT = 'online';
static  OFFLINE_CONTENT = 'Connect to Internet';
static  CURRENT_USER_TOKEN_KEY = 'TOKEN';
static  APP_VERSION_KEY = 'VERSION';
static  LAST_LOGGED_OUT_TIME_KEY = 'TIME';
static  USER_EMAIL_KEY = 'EMAIL';
static  USER_ID_KEY = 'ID';
static  USER_NAME_KEY = 'NAME';
static  USER_PHONE_NUMBER_KEY = 'PHONE_NUMBER';
static  USER_PROFILE_KEY = 'PROFILE';
static  USER_LOGIN_SUCCESSFUL_MESSAGE = 'Login Successful';
static  APP_VERSION_URL_PARAMS = 'api/version';
static  APP_EVENT_EMIT_VALUE_KEY = 'OUTPUT';
static  APP_EVENT_INPUT_BIND_VALUE_KEY = 'INPUT';
static  APP_EVENT_LABEL_NAME_KEY = 'LABEL';
static  APP_EVENT_INPUT_OPTIONS_KEY = 'OPTIONS';
static  API_UPDATE_URL_PARAMS = 'app/accident/update';
static  API_ACCIDENT_CREATE_URL_PARAMS = 'app/accident/create';
static  UPLOAD_QUEUE_KEY = 'uploadQueue';
static  DATE_KEY = 'datekey';
static  IMAGE_KEY = 'imagekey';
static  AUDIO_KEY = 'audiokey';
static  VIDEO_KEY = 'videokey';
static  UPLOAD_PARAMS_KEY = 'media/';
static  API_VERSION_URL_PARAMS = 'api/user/logout';
static  API_LOGOUT_URL_PARAMS = 'api/user/logout';
static  API_ACCIDENT_URL_PARAMS = 'app/accident';
static  API_ACCIDENT_LIST_URL_PARAMS = 'app/accident/list';
static  DATA_NOT_FOUND_MESSAGE = 'Respective Date Entry Not Found';
static  FUTURE_DATE_NOT_ALLOWED_MESSAGE = 'Future Date Could Not be Allowed';
static  ERROR_GETTING_DATE_MESSAGE = 'Error occurred while getting date';
static  SERVER_NOT_REACHABLE_MOVE_TO_OFFLINE_MODE = 'Server Is Not Reachable, Please Move To Offline Mode';
static  INVESTIGATION_OFFICER_VALIDATION_MESSAGE = 'Investigation Officer Name Could Not be Empty!';
static  INVESTIGATION_DATE_VALIDATION_MESSAGE = 'Accident Date Could Not be Empty!';
static  ACCIDENT_ID_NOT_UPDATED = 'Accident Id Could Not be Empty!';
static  IMAGES_UPLOADING_MESSAGE = 'Please Wait Image File is Uploading...';
static  VIDEOS_UPLOADING_MESSAGE = 'Please Wait Video File is Uploading...';
static  EVEN_EMITTER_IMAGE_FOR_IMAGE_PATH_EMIT_KEY = 'EmitValue';
static  EVEN_EMITTER_IMAGE_EMIT_KEY = 'returnValue';
static  EVEN_EMITTER_IMAGE_BIND_KEY = 'bindValue';
static  EVEN_EMITTER_IMAGE_LABEL_KEY = 'labelName';
static  EVEN_EMITTER_VIDEO_EMIT_KEY = 'returnValue';
static  EVEN_EMITTER_VIDEO_BIND_KEY = 'bindValue';
static  EVEN_EMITTER_VIDEO_LABEL_KEY = 'labelName';

static  EVEN_EMITTER_STRING_EMIT_KEY = 'returnValue';
static  EVEN_EMITTER_STRING_BIND_KEY = 'bindValue';
static  EVEN_EMITTER_STRING_LABEL_KEY = 'labelName';
static  GPS_LOCATION_LOADING_MESSAGE = 'Please wait getting Lat & Long..';
static  ACCIDENT_DETAILS_LOADING_MESSAGE = 'Please wait getting Accident Details..';



}