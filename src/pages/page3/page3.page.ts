import { UrlService } from "src/providers/url-service";
import { Component, OnInit } from "@angular/core";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { Router } from "@angular/router";
import { Network } from "@ionic-native/network/ngx";
import { StorageService } from "src/providers/storageServices";
import { SchemaModels } from "src/providers/schemaObject";
import { ServicesAlertsProviderService } from "src/providers/services-alerts-provider.service";
import Util from "../../constants/content";
import { MenuController } from "@ionic/angular";
import { Subscription } from "rxjs";
import { Storage } from "@ionic/storage";
@Component({
  selector: "app-page3",
  templateUrl: "./page3.page.html",
  styleUrls: ["./page3.page.scss"]
})
export class Page3Page implements OnInit {
  pendingCount: any;
  internetStatus;
  value;
  today = new Date();
  Unsubscribe: Subscription;
  public previousStatus: any;
  public dateOfToday = new Date().toISOString().slice(0, 10);
  local: any;

  constructor(
    private datePicker: DatePicker,
    private route: Router,
    private network: Network,
    public urlService: UrlService,
    public schemaService: SchemaModels,
    private menuCtrl: MenuController,
    public storageService: StorageService,
    public storage: Storage,
    public alertService: ServicesAlertsProviderService
  ) {
    this.menuCtrl.enable(true);
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.getCurrentNetworkStatus();
    this.urlService.onStorageProfileDetails();
  }
  async ngOnInit() {
    this.pendingCount = await this.storageService.getListPendingCountWithoutObservable();
    this.Unsubscribe = await this.storageService.offlineStatusCountOb.subscribe(
      count => {
        this.pendingCount = count;
        console.log("homepage offlinecount", this.pendingCount);
      }
    );
  }

  ionViewDidEnter() {
    try {
      debugger;
      this.urlService.onchangeStatus();
      this.Unsubscribe = this.urlService.networkConnectDisConnect.subscribe(
        async data => {
          this.internetStatus = await data;
          this.alertService.showToast(this.internetStatus, Util.TOAST_DURATION);
          // this.displayNetworkUpdate(this.previousStatus);
          if (this.internetStatus === "online") {
            if (this.pendingCount != 0) {
              this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
                const prof = await JSON.parse(profile);
                if (prof) {
                  if (prof.profiledetails) {
                    this.storageService.token = await prof.profiledetails.token;
                    this.storageService.getUploadQueue();
                  }
                }
              });
            }
          }
        }
      );
    } catch (error) {}
  }

  ionViewDidLeave() {
    if (this.Unsubscribe) {
      this.Unsubscribe.unsubscribe();
    }
  }
  /**
   * Use This Add Accident Entry Record!
   */
  addEntry() {
    try {
      this.schemaService.resetForm();
      this.storageService.selectedDate = new Date().toDateString();
      //   this.user._id = "";
      this.datePicker
        .show({
          date: new Date(),
          mode: "date",
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        })
        .then(
          date => {
            
            if (date <= this.today) {
              this.storageService.selectedDate = date.toDateString();
              this.urlService.selectedDate = date.toDateString();
              this.schemaService.acciDent.page1.selectedDate = date.toDateString();
              this.storageService.update = false;
             
              this.route.navigate(["/page1"]);
            } else {
            
              this.alertService.showToast(
                Util.FUTURE_DATE_NOT_ALLOWED_MESSAGE,
                Util.TOAST_DURATION
              );
            }
          },
          err => {   this.alertService.dismissLoading(),
          console.log(Util.ERROR_GETTING_DATE_MESSAGE, err);
          }
        );
    } catch (error) {}
  }
  /**
   *  Show List of Accident Details!
   */
  viewEntry() {
    try {
      this.schemaService.resetForm();
      if (this.network.type != "none") {
        this.datePicker
          .show({
            date: new Date(),
            mode: "date",
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
          })
          .then(
            date => {
              if (date <= this.today) {
                this.storageService.selectedDate = date.toDateString();
                this.urlService.selectedDate = date.toDateString();
                this.urlService.onServerCheck().then(async serverStatus => {
                  if (serverStatus === true) {
                    const dateformat = new Date().toDateString();
                    this.storageService.selectedDate = await dateformat;
                    await this.urlService.getAccidents().then((status: boolean) => {
                     
                        if (status === false) {
                          this.alertService.showToast(
                            Util.DATA_NOT_FOUND_MESSAGE,
                            Util.TOAST_DURATION
                          );
                        } else {
                          this.route.navigate(["/view-list-entry"]);
                        }
                      
                  });
                  } else {
                    this.alertService.dismissLoading();
                    this.alertService.showToast(
                      Util.SERVER_NOT_AVAILABLE,
                      Util.TOAST_DURATION
                    );
                  }
                });
              } else {
                this.alertService.dismissLoading();
                this.alertService.showToast(
                  Util.FUTURE_DATE_NOT_ALLOWED_MESSAGE,
                  Util.TOAST_DURATION
                );
              }
            },
            err => console.log(Util.ERROR_GETTING_DATE_MESSAGE, err)
          );
      } else {
        const networkStatus = this.getCurrentNetworkStatus();
        this.alertService.showToast(networkStatus, Util.TOAST_DURATION);
      }
    } catch (error) {}
  }

  getCurrentNetworkStatus() {
    if (this.network.type === "none") {
      return (this.internetStatus = Util.OFFLINE_CONTENT);
    } else {
      return (this.internetStatus = Util.ONLINE_CONTENT);
    }
  }
 
}
