import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MenuController } from "@ionic/angular";
import { UrlService } from "src/providers/url-service";
import { ServicesAlertsProviderService } from "src/providers/services-alerts-provider.service";
import Util from "../../constants/content";
import { Subscription } from "rxjs";
import { StorageService } from "src/providers/storageServices";
@Component({
  selector: "app-view-list-entry",
  templateUrl: "./view-list-entry.page.html",
  styleUrls: ["./view-list-entry.page.scss"]
})
export class ViewListEntryPage implements OnInit {
  choosenDate: any;
  Unsubscribe: Subscription;

  constructor(
    public route: Router,
    private menuCtrl: MenuController,
    private alertService: ServicesAlertsProviderService,
    public urlService: UrlService,
    private storageService: StorageService
  ) {
    this.menuCtrl.enable(true);
  }

  ngOnInit() {}
  
  close() {
    this.route.navigate(["/dashboard"]);
  }

  async ionViewWillEnter() {
    debugger;
    await this.urlService.onStorageProfileDetails();
    //await this.urlService.getAccidents();
    this.choosenDate = this.urlService.selectedDate;
  }
  openEntry(entry) {
    this.urlService.getAccident(entry._id).then(status => {
      this.storageService.update = true;
      this.route.navigate(["/page1"]);
    });
  }

  ionViewDidLeave() {
    if(this.Unsubscribe) {
    this.Unsubscribe.unsubscribe();
    }
  }
}
