import {
  Component,
  OnInit,
  ViewChild,
  Input,
  ContentChild
} from '@angular/core';
import { SchemaModels } from 'src/providers/schemaObject';
import { Router } from '@angular/router';
import { NavController, MenuController } from '@ionic/angular';
import { ServicesAlertsProviderService } from 'src/providers/services-alerts-provider.service';
import Util from '../../constants/content';
import { StorageService } from 'src/providers/storageServices';
import { Network } from '@ionic-native/network/ngx';
import { UrlService } from 'src/providers/url-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.page.html',
  styleUrls: ['./page1.page.scss']
})
export class Page1Page implements OnInit {
  @ViewChild(ContentChild) content: ContentChild;
  @Input('returnValue') value: any;
  statusValue: boolean;
  accidentSeverity = [
    'Fatal',
    'Grievous Injury',
    'Simple Injury (Hospitalized)',
    'Simple Injury (Non-Hospitalized)',
    'Vehicle Damage(No Injury)'
  ];
 public textString = '';
  constructor(
    public schemaObject: SchemaModels,
    private storageService: StorageService,
    public route: Router,
    private network: Network,
    public urlService: UrlService,
    private menuCtrl: MenuController,
    public storage: Storage,
    public alertService: ServicesAlertsProviderService
  ) {
    this.menuCtrl.enable(true);
  
  }

  ngOnInit() {
    this.urlService.onStorageProfileDetails();
  }
  /**
   *  LifeCycle Hook Find the Boolean Status!
   */
 async ionViewWillEnter() {
    this.statusValue = await this.storageService.update;
    if (this.statusValue === true) {
      this.textString = 'Update';
    } else {
      this.textString = 'Submit';
    }
  }
  /**
   * Use this Method To Navigate Next Screen!
   */
   right() {
   
    const invest =  this.schemaObject.acciDent.page1.investigatingOfficer;
    const dateEntry =  this.schemaObject.acciDent.page1.accidentDate;
    if (!invest) {
      this.alertService.genericAlertMessage(
        Util.INVESTIGATION_OFFICER_VALIDATION_MESSAGE,
        'info'
      );
    } else if (!dateEntry) {
      this.alertService.genericAlertMessage(
        Util.INVESTIGATION_DATE_VALIDATION_MESSAGE,
        'info'
      );
    } else {
      this.route.navigate(['/page2']);
    }
  }
   /**
   * find the  submit or update mehtod
   */
  validateMethod() {
    debugger
    if (this.textString === 'Update') {
      this.storageService.onUpdate();
      this.route.navigate(['/dashboard']);
    } else {
      this.storageService.submitForm();
      this.route.navigate(['/dashboard']);
    }
  }
  /**
   * Use This Method to Submit Data To DB!
   */
 async submit() {
    const invest = await this.schemaObject.acciDent.page1.investigatingOfficer;
    const dateEntry = await this.schemaObject.acciDent.page1.accidentDate;
    if (!invest) {
      this.alertService.genericAlertMessage(
        Util.INVESTIGATION_OFFICER_VALIDATION_MESSAGE,
        'info'
      );
    } else if (!dateEntry) {
      this.alertService.genericAlertMessage(
        Util.INVESTIGATION_DATE_VALIDATION_MESSAGE,
        'info'
      );
    } else {
      if (this.network.type != 'none') {
        this.urlService.onServerCheck().then((status: boolean) => {
          if (status === true) {
            this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
              const prof = await JSON.parse(profile);
              if (prof) {
                if (prof.profiledetails) {
                  this.storageService.token = await prof.profiledetails.token;
                  this.validateMethod();
                }
              }
            });
          } else {
            this.alertService.showToast(
              Util.SERVER_NOT_REACHABLE_MOVE_TO_OFFLINE_MODE,
              Util.TOAST_DURATION
            );
          }
        });
      } else {
        this.storageService.addToUploadQueue();
        this.route.navigate(['/dashboard']);
      }
    }
  }
   /**
   * Use This Method to Navigate To  Back!
   */
  left() {
    // view-list-entry
    if(this.statusValue === true){
      this.route.navigate(['/view-list-entry']);
    }else {
      this.route.navigate(['/dashboard']);
    }
   
  }
}
