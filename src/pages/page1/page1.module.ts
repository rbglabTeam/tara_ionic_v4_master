import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Page1Page } from './page1.page';
import { AnyoneCComponent } from 'src/components/anyone-c/anyone-c.component';
import { LocationCComponent } from 'src/components/location-c/location-c.component';
import { StringelementComponent } from 'src/components/stringelement/stringelement.component';
import { NumberelementComponent } from 'src/components/numberelement/numberelement.component';
import { MultiComponent } from 'src/components/multi/multi.component';
import { DatetimeComponent } from 'src/components/datetime/datetime.component';
import { VideoCComponent } from 'src/components/video-c/video-c.component';
import { AudioCComponent } from 'src/components/audio-c/audio-c.component';
import { BooleanCComponent } from 'src/components/boolean-c/boolean-c.component';
import { RadioCComponent } from 'src/components/radio-c/radio-c.component';

const routes: Routes = [
  {
    path: '',
    component: Page1Page
  }
];

@NgModule({
  imports: [
    FormsModule,
    IonicModule,
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Page1Page, DatetimeComponent, VideoCComponent,
    LocationCComponent, StringelementComponent, NumberelementComponent,
     MultiComponent, AnyoneCComponent,AudioCComponent, BooleanCComponent,RadioCComponent ]
  
  })
export class Page1PageModule {}
