import { TestBed } from '@angular/core/testing';

import { ServicesAlertsProviderService } from './services-alerts-provider.service';

describe('ServicesAlertsProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicesAlertsProviderService = TestBed.get(ServicesAlertsProviderService);
    expect(service).toBeTruthy();
  });
});
