import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Network } from "@ionic-native/network/ngx";
import { ServicesAlertsProviderService } from "./services-alerts-provider.service";
import { Storage } from "@ionic/storage";
import { Subject } from "rxjs";
import {
  FileTransfer,
  FileTransferObject,
  FileUploadOptions
} from "@ionic-native/file-transfer/ngx";
import { SchemaModels } from "./schemaObject";
import Util from "../constants/content";
import { UrlService } from "./url-service";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  networkConnectDisConnectOb = new Subject<any>();
  offlineStatusCountOb = new Subject<any>();
  sendSignle = [];
  countArray = [];
  imageArray = [];
  videoArray = [];
  audioArray = [];
  checkDataMode = false;
  token;
  refreshAppQueueStatus = false;
  selectedDate;
  arrayLengthCount = 0;
  update = false;

  constructor(
    private http: HttpClient,
    private network: Network,
    private alertService: ServicesAlertsProviderService,
    private transfer: FileTransfer,
    private storage: Storage,
    private urlService: UrlService,
    private schemaService: SchemaModels
  ) {}
  /**
   * send data and images, video and audio to database!
   */
  submitForm() {
    debugger;
    try {
      if (this.checkDataMode === false) {
        debugger;
        this.sendSignle = [];

        this.sendSignle.push(this.schemaService.acciDent);

        const packet = {
          duration_date_time: this.selectedDate,
          data: this.schemaService.acciDent,
          token: this.token
        };
        console.log("submitmethod", packet.data);
        this.http
          .post<{ code: any; status: any }>(
            Util.SANTHOSH_API + Util.API_ACCIDENT_CREATE_URL_PARAMS,
            packet
          )
          .subscribe(
            async res => {
              if (res.code === 200) {
                this.alertService.showToast(res.status, Util.TOAST_DURATION);
                const removekey = this.storage
                  .remove(Util.UPLOAD_QUEUE_KEY)
                  .then(async sucess => {
                    console.log("remove uploadqueue objects", removekey);
                    await this.resetAllValues();
                    await this.onOfflineuploadDataImage();
                  });
              } else {
                this.alertService.showToast(res.status, Util.TOAST_DURATION);
              }
            },
            err => {
              this.refreshAppQueueStatus = true;
              this.getStorageData();
              console.log(err);
            }
          );
      } else {
        debugger;
        this.storage.get(Util.UPLOAD_QUEUE_KEY).then(data => {
          JSON.parse(JSON.stringify(data)).forEach(element => {
            const packet = {
              duration_date_time: element.page1.selectedDate,
              data: element,
              token: this.token
            };
            debugger;
            console.log("submitmethod", packet.data);
            this.http
              .post<{ status: any; code: any }>(
                Util.SANTHOSH_API + Util.API_ACCIDENT_CREATE_URL_PARAMS,
                packet
              )
              .subscribe(
                async res => {
                  if (res.code === 200) {
                    console.log("submitresp", res["code"]);
                    this.alertService.showToast(res.status, Util.TOAST_DURATION);
                    const removekey = await this.storage
                      .remove(Util.UPLOAD_QUEUE_KEY)
                      .then(async sucess => {
                        this.arrayLengthCount = 0;
                        await this.offlineStatusCountOb.next(
                          this.arrayLengthCount
                        );
                        await this.offlineStatusCountOb.asObservable();
                        await this.resetAllValues();
                        await this.onOfflineuploadDataImage();
                        console.log("removekeystatys", sucess);
                      });
                  } else {
                    this.alertService.showToast(res.status, Util.TOAST_DURATION);
                  }
                },
                err => {
                  this.refreshAppQueueStatus = true;
                  this.getStorageData();
                  console.log(err);
                }
              );
          });
        });
      }
    } catch (error) {}
  }
  /**
   * update data in database
   */
  onUpdate() {
    debugger;
    try {
      if (this.urlService._id) {
        const packet = {
          accident_id: this.urlService._id,
          data: this.schemaService.acciDent,
          token: this.token
        };
        this.schemaService.resetForm();
        this.http
          .post<{ status: any }>(
            Util.SANTHOSH_API + Util.API_UPDATE_URL_PARAMS,
            packet
          )
          .subscribe(
            res => {
              console.log(res);
              this.urlService._id = "";
              this.alertService.showToast(res.status, Util.TOAST_DURATION);
            },
            err => {
              console.log(err);
            }
          );
      } else {
        this.alertService.showToast(Util.ACCIDENT_ID_NOT_UPDATED, Util.TOAST_DURATION);
      }
    } catch (error) {}
  }
  /**
   * get storage object count
   */
  getStorageData() {
    debugger;
    this.storage.get(Util.UPLOAD_QUEUE_KEY).then(async data => {
      if (data) {
        this.countArray = [];
        JSON.parse(JSON.stringify(data)).forEach(element => {
          this.countArray.push(element);
          this.arrayLengthCount = this.countArray.length;
          console.log("storagecount", this.arrayLengthCount);
          this.offlineStatusCountOb.next(this.arrayLengthCount);
          this.offlineStatusCountOb.asObservable();
        });
      }
    });
  }
  getStorageDate() {
    this.storage.get(Util.DATE_KEY).then(data => {
      if (data) {
        this.selectedDate = data;
      }
      console.log("datebckup", data);
    });
  }
  /**
   * get  images path when stored in offline
   */
  onOfflineuploadDataImage() {
    try {
      debugger;
      this.storage.get(Util.IMAGE_KEY).then(async data => {
        if (data) {
          await JSON.parse(JSON.stringify(data)).forEach(async element => {
            const type = "image";
            await this.uploadFileImage(element.image, element.imagepath, type);
            console.log("getofflineimage", element);
          });
        }
        await this.storage.remove(Util.IMAGE_KEY).then(async sucess => {
          this.imageArray = [];
          await this.onOfflineuploadDataVideo();
        });
      });
    } catch (error) {}
  }
  /**
   * get  audio path when stored in offline
   */
  onOfflineuploadDataAudio() {
    try {
      this.storage.get(Util.AUDIO_KEY).then(async data => {
        if (data) {
          await JSON.parse(JSON.stringify(data)).forEach(async element => {
            const type = "audio";
            await this.uploadFile(element.audio, element.audiopath, type);
          });
        }
        this.audioArray = [];
        await this.storage.remove(Util.AUDIO_KEY);

        await this.onOfflineuploadDataVideo();
      });
    } catch (error) {}
  }
  /**
   * get  videos path when stored in offline
   */
  onOfflineuploadDataVideo() {
    try {
      this.storage.get(Util.VIDEO_KEY).then(async data => {
        if (data) {
          await JSON.parse(JSON.stringify(data)).forEach(async element => {
            const type = "video";
            await this.uploadFile(element.video, element.videopath, type);
          });
        }
        this.videoArray = [];
        await this.storage.remove(Util.VIDEO_KEY);
      });
    } catch (error) {}
  }
  /**
   *
   * @param image save  images path when store in offline
   */
  onSaveOfflineImage(image) {
    try {
      this.imageArray.push(image);
      this.storage.set(Util.IMAGE_KEY, this.imageArray).then(async success => {
        console.log("offlineuploadImageStatus", success);
      });
    } catch (error) {}
  }
  /**
   *
   * @param image save  videos path when store in offline
   */
  onSaveOfflineVideo(video) {
    try {
      this.videoArray.push(video);
      this.storage.set(Util.VIDEO_KEY, this.videoArray).then(async success => {
        console.log("offlineuploadVideoStatus", success);
      });
    } catch (error) {}
  }
  /**
   *
   * @param image save  audio path when store in offline
   */
  onSaveOfflineAudio(audio) {
    try {
      this.audioArray.push(audio);
      this.storage.set(Util.AUDIO_KEY, this.audioArray).then(async success => {
        console.log("offlineuploadAudioStatus", success);
      });
    } catch (error) {}
  }
  /**
   * save accidents object in offline storage
   */
  async addToUploadQueue() {
    debugger;
    try {
      let shuffleArray = [];
      if (
        this.schemaService.acciDent != null &&
        this.schemaService.acciDent.page1.selectedDate != null
      ) {
        shuffleArray.push(this.schemaService.acciDent);
        shuffleArray.forEach(element => {
          this.countArray.push(element);
          console.log("shuffleArray", this.countArray, shuffleArray.length);
          this.arrayLengthCount = this.countArray.length;
        });
        this.storage
          .set(Util.UPLOAD_QUEUE_KEY, this.countArray)
          .then(async success => {
            console.log("offlineuploadmethod", success);
          });
        debugger;
        await this.offlineStatusCountOb.next(this.arrayLengthCount);
        await this.offlineStatusCountOb.asObservable();
        this.schemaService.resetForm();
      } else {
        alert("Accident field Should not be Empty!");
      }
    } catch (error) {}
  }
  /**
   * Offline saved images and videos push to server when internet comes up
   */
  async getUploadQueue() {
    try {
      debugger;
      if (this.countArray.length > 0) {
        console.log("getuploadqueuecheckObjectDetails", this.countArray);
        this.countArray = [];
        // await this.getListPendingCount();
        this.checkDataMode = true;
        await this.schemaService.resetForm();
        await this.submitForm();
      } else {
        console.log("getuploadqueuecheck");
      }
    } catch (error) {}
  }
  /**
   * object count changed observable automatically updated!
   */
  async getListPendingCountWithoutObservable() {
    return await this.arrayLengthCount;
  }
  async uploadFile(data, path, type) {
    const fileTransfer: FileTransferObject = await this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: type,
      fileName: data.name,
      chunkedMode: false,
      mimeType: data.type,
      headers: {}
    };
    const filePath = data.fullPath;
    console.log("services", filePath);
    fileTransfer
      .upload(
        filePath,
        Util.SANTHOSH_API + "media/" + type + "?" + type + "=" + path,
        options
      )
      .then(
        async data => {
          await this.alertService.showToast(type + "uploaded", Util.TOAST_DURATION);
        },
        err => {
          // this.loading = null;
          console.log(err);
        }
      );
  }
  async uploadFileImage(data, path, type) {
    debugger;
    const fileTransfer: FileTransferObject = await this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: type,
      fileName: data,
      chunkedMode: false,
      mimeType: type,
      headers: {}
    };
    const filePath = path;
    console.log("services", filePath);
    fileTransfer
      .upload(
        filePath,
        Util.SANTHOSH_API + "media/image?image=" + path,
        options
      )
      .then(
        async data => {
          await this.alertService.showToast(type + "uploaded", Util.TOAST_DURATION);

          // console.log('imageuploadedResponse'y, JSON.parse(data['response']))
        },
        err => {
          // this.loading = null;
          console.log(err, "upload image error response");
        }
      );
  }

 
  resetAllValues() {
    this.schemaService.resetForm();
    this.sendSignle = [];
    this.countArray = [];
  }
}
